import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { InteractionModule } from '../interaction/interaction.module';
import { FormModule } from '../form/form.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { CardComponent } from './card/card.component';
import { ComponentAdderComponent } from './component-adder/component-adder.component';
import { ViewComponent } from './view/view.component';
import { MaterialModule } from '../main/material.module';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list/list-item/list-item.component';
import { ExpansionComponent } from './expansion/expansion.component';
import { SelectComponent } from './select/select.component';


@NgModule({
  declarations: [
    CardComponent,
    ComponentAdderComponent,
    ExpansionComponent,
    ListItemComponent,
    ListComponent,
    SelectComponent,
    SidenavComponent,
    ToolbarComponent,
    ViewComponent
  ],
  entryComponents: [
    ViewComponent
  ],
  exports: [
    SidenavComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    InteractionModule,
    FormModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class LayoutModule { }
