import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { StateService } from '../../main/state-manager/state.service';


@Component({
  selector: 'app-expansion', //tslint:disable-line
  templateUrl: './expansion.component.html',
  styleUrls: ['./expansion.component.css']
})
export class ExpansionComponent extends ConfigurableComponent implements OnInit {

  explanation = 'An expansion to hold components';
  name = 'Expansion';

  availableHeaderComponents = [
    'Button',
    'Header'
  ];

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }

  private insertChildNodes(): void {
    this.stateService.insertNode({
      name: 'Header',
      id: this.id + '-header',
      children: [],
      config: {}
    }, this.id);

    this.stateService.insertNode({
      name: 'Content',
      id: this.id + '-content',
      children: [],
      config: {}
    }, this.id);
  }

  ngOnInit(): void {
    this.insertChildNodes();
  }
}
