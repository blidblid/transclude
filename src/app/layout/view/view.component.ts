import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  id: string;

  availableComponents = [
    'Card',
    'Youtube',
    'Expansion'
  ];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.id = this.router.url.replace('/', '');
  }
}
