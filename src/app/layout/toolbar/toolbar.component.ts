import { Component, ViewEncapsulation, OnInit, Input, HostBinding } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';


@Component({
  selector: 'app-toolbar', //tslint:disable-line
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarComponent extends ConfigurableComponent {

  explanation = 'A toolbar to overview the app';
  name = 'Toolbar';
  id = 'toolbar';

  availableComponents: string[] = [
    'Button',
    'Header',
    'Input',
    'Select',
    'Slide toggle',
    'Slider'
  ];

  @Input()
  reverseSidenavOpener: boolean = false;

  @Input()
  @HostBinding('class.hidden')
  hidden: boolean = false;

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService) {
    super(bottomSheet, settingsService);
  }
}
