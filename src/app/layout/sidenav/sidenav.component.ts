import {
  Component,
  Input,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  OnInit,
  HostBinding
} from '@angular/core';
import { MatBottomSheet, MatSidenav } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';


@Component({
  selector: 'app-sidenav', //tslint:disable-line
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent extends ConfigurableComponent implements OnDestroy, OnInit {

  explanation = 'A sidenav used for quick app navigation';
  name = 'Sidenav';
  id = 'sidenav';

  /** Whether there should be a gap that accounts for the toolbar. */
  @Input()
  gap: boolean = false;

  @Input()
  @HostBinding('class.hidden')
  hidden: boolean = false;

  @ViewChild(MatSidenav)
  sidenav: MatSidenav;

  availableComponents: string[] = [
    'Button',
    'Header',
    'Input',
    'List',
    'Paragraph',
    'Select',
    'Slide toggle',
    'Slider',
    'Textarea'
  ];

  mobileQuery: MediaQueryList;

  mobileQueryListener: () => void;

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher) {
    super(bottomSheet, settingsService);
  }

  /** Toggle the sidenav. */
  toggle(): void {
    this.sidenav.toggle();
  }

  /** Opens the sidenav. */
  open(): void {
    this.sidenav.open();
  }

  private setupMobileQueries(): void {
    this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  ngOnInit() {
    this.setupMobileQueries();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }
}
