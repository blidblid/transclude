import { Component, ElementRef } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../../main/component-manager/configurable.component';
import { SettingsService } from '../../../main/settings-manager/settings.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-item', //tslint:disable-line
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent extends ConfigurableComponent {

  explanation = 'A list item used in lists.';
  name = 'List item';

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private router: Router,
    private elementRef: ElementRef) {
    super(bottomSheet, settingsService);
  }
}
