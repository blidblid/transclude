import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';

@Component({
  selector: 'app-list', //tslint:disable-line
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends ConfigurableComponent implements OnInit {

  explanation = 'A list that holds list items.';
  name = 'List';

  availableComponents = [
    'List item'
  ];

  constructor(private bottomSheet: MatBottomSheet, private settingsService: SettingsService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit(): void {
    this.spanRow = true;
  }
}
