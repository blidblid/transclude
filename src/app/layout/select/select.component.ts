import { Component, OnInit, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { FormControl } from '@angular/forms';
import { StateService } from '../../main/state-manager/state.service';


@Component({
  selector: 'app-select', //tslint:disable-line
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent extends ConfigurableComponent implements OnInit {

  name = 'Select';
  explanation = 'Select to be used in forms';

  set value(value: any) {
    this.stateService.setValue(this.id, value);
  }

  get value(): any {
    return this.stateService.getValue(this.id);
  }

  @HostBinding('attr.formControlName') formControlName: string = 'test';

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit(): void {
    this['test'] = new FormControl('');
  }
}
