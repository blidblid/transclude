import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { StateService } from '../../main/state-manager/state.service';

@Component({
  selector: 'app-card', //tslint:disable-line
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent extends ConfigurableComponent implements OnInit {

  explanation = 'A card used to hold content.';
  name = 'Card';

  availableHeaderComponents: string[] = [
    'Button',
    'Header',
    'Input',
    'Select',
    'Slide toggle',
    'Slider'
  ];

  availableContentComponents: string[] = [
    'Button',
    'Expansion',
    'Header',
    'Input',
    'List',
    'Paragraph',
    'Select',
    'Slide toggle',
    'Slider',
    'Textarea',
    'Youtube'
  ];

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit(): void { }
}
