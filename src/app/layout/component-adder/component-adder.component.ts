import { Component, Input, OnInit } from '@angular/core';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { StateService, StateNode } from '../../main/state-manager/state.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-component-adder', //tslint:disable-line
  templateUrl: './component-adder.component.html',
  styleUrls: ['./component-adder.component.css']
})
export class ComponentAdderComponent implements OnInit {

  state: Observable<StateNode>;

  @Input()
  parentId: string;

  @Input()
  icon: string = 'edit';

  destroy: Function = id => this.stateService.removeNode(id);

  @Input()
  availableComponents: string[] = [
    'Button',
    'Header',
    'Input',
    'List',
    'Paragraph',
    'Select',
    'Slide toggle',
    'Slider',
    'Textarea',
    'Youtube'
  ];

  disableEdit: Observable<boolean>;

  constructor(
    private settingsService: SettingsService,
    private stateService: StateService) { }

  /** Adds a component to the state. */
  addComponent(componentName: string): void {
    // Insert the component into the state tree.
    const generatedId = this.stateService.generateComponentId();
    this.stateService.insertNode({
      id: generatedId,
      name: componentName,
      children: this.getChildren(componentName, generatedId),
      config: this.getConfig(componentName)
    }, this.parentId);
  }

  onClick(event: Event): void {
    event.stopPropagation();
  }

  /** Returns default configs for new components. */
  private getConfig(componentName: string): Config {
    if (componentName === 'Input') {
      return {
        label: toJson(new Label())
      };
    } else if (componentName === 'Select') {
      return {
        label: toJson(new Label()),
        disableRipple: toJson(new DisableRipple())
      };
    } else if (componentName === 'Slide toggle') {
      return {
        label: toJson(new Label()),
        disableRipple: toJson(new DisableRipple())
      };
    } else if (componentName === 'Slider') {
      return {
        label: toJson(new Label())
      };
    } else if (componentName === 'Textarea') {
      return {
        label: toJson(new Label())
      };
    } else if (componentName === 'Button') {
      return {
        color: toJson(new Color()),
        content: toJson(new Content()),
        disableRipple: toJson(new DisableRipple()),
        action: toJson(new Action())
      };
    } else if (componentName === 'Header') {
      return {
        content: toJson(new Content()),
        font: toJson(new Font()),
        spanColumns: toJson(new SpanColumns('Entire row'))
      };
    } else if (componentName === 'Paragraph') {
      return {
        content: toJson(new Content()),
        font: toJson(new Font()),
        spanColumns: toJson(new SpanColumns('Entire row'))
      };
    } else if (componentName === 'Youtube') {
      return {
        video: toJson(new Video()),
        spanColumns: toJson(new SpanColumns('One column'))
      };
    } else if (componentName === 'Card') {
      return {
        spanColumns: toJson(new SpanColumns('One column'))
      };
    } else if (componentName === 'Expansion') {
      return {
        spanColumns: toJson(new SpanColumns('One column'))
      };
    } else if (componentName === 'List') {
      return {
        spanColumns: toJson(new SpanColumns('Entire row'))
      };
    } else if (componentName === 'List item') {
      return {
        content: toJson(new Content()),
        action: toJson(new Action())
      };
    } else {
      throw new Error('No default config found for componenent with name ' + componentName);
    }
  }

  /** Returns default children for new components. */
  private getChildren(componentName: string, id: string): StateNode[] {
    if (componentName === 'Card') {
      return [
        {
          name: 'header',
          config: {},
          id: id + '-header',
          children: []
        }, {
          name: 'content',
          config: {},
          id: id + '-content',
          children: []
        }
      ];
    }
    return [];
  }

  ngOnInit(): void {
    this.disableEdit = this.settingsService.disableEdit;
    this.state = this.stateService.stateById(this.parentId);
  }
}

class Config {
  content?: Content;
  label?: Label;
  action?: Action;
  padding?: Padding;
  spanColumns?: SpanColumns;
  font?: Font;
  video?: Video;
  color?: Color;
  disableRipple?: DisableRipple;
}

class Content {
  value = 'Content';
  name = 'Content';
  key = 'content';
  type = 'string';
}

class Padding {
  value = 16;
  key = 'padding';
  name = 'Padding';
  type = 'number';
}

class DisableRipple {
  value = false;
  key = 'disableRipple';
  name = 'Disable ripple';
  type = 'option';
  options = [
    false,
    true
  ];
}

class SpanColumns {
  value = 'Entire row';
  key = 'spanColumns';
  name = 'Span columns';
  type = 'option';
  options = [
    'One column',
    'Two columns',
    'Three columns',
    'Four columns',
    'Entire row'
  ];

  constructor(defaultValue: string) {
    this.value = defaultValue;
  }
}

class Font {
  value = 'h1';
  key = 'font';
  name = 'Font';
  type = 'option';
  options = [
    'h1',
    'h2',
    'h3',
    'h4',
    'h5'
  ];
}

class Video {
  value = '9XblbvrmgcM';
  name = 'Video ID';
  key = 'id';
  type = 'string';
}

class Color {
  value = 'none';
  name = 'Color';
  key = 'color';
  type = 'option';
  options = [
    'none',
    'primary',
    'accent',
    'error',
    'warn'
  ];
}

class Action {
  value = [];
  actions = [];
  name = 'Action';
  key = 'action';
  type = 'action';
  options = ['Navigate'];
}

class Label {
  value = 'Label';
  name = 'Label';
  key = 'label';
  type = 'string';
}

function toJson(object: any) {
  return (JSON.parse(JSON.stringify(object)));
}
