import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentAdderComponent } from './component-adder.component';

describe('ComponentAdderComponent', () => {
  let component: ComponentAdderComponent;
  let fixture: ComponentFixture<ComponentAdderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentAdderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentAdderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
