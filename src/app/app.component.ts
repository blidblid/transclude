import { Component } from '@angular/core';
import { FirebaseService } from './main/firebase-manager/firebase.service';
import { StateService } from './main/state-manager/state.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root', //tslint:disable-line
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private firebase: FirebaseService,
    private stateService: StateService) {
    this.firebase.userApps.pipe(take(1))
      .subscribe(apps => {
        if (apps && apps[0]) {
          this.stateService.loadApp(apps[0]);
        }
      });
  }
}
