import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ButtonComponent } from './button/button.component';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from '../main/material.module';
import { YoutubeComponent } from './youtube/youtube.component';
import { YoutubePipe } from './youtube/youtube.pipe';


@NgModule({
  declarations: [
    ButtonComponent,
    HeaderComponent,
    ParagraphComponent,
    YoutubeComponent,
    YoutubePipe
  ],
  exports: [
    ButtonComponent,
    HeaderComponent,
    ParagraphComponent,
    YoutubeComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class InteractionModule { }
