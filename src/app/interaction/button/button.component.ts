import { Component } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { ActionService } from '../../main/action.service';


@Component({
  selector: 'app-button', //tslint:disable-line
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent extends ConfigurableComponent {

  explanation = 'A simple button used to trigger actions';
  name = 'Button';

  runActions(): void {
    this.config.action.actions.forEach(action => {
      this.actionService.run(action);
    });
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private actionService: ActionService) {
    super(bottomSheet, settingsService);
  }
}
