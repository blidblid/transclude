import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';

@Component({
  selector: 'app-header', //tslint:disable-line
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends ConfigurableComponent implements OnInit {

  explanation = 'A header that holds text.';
  name = 'Header';

  constructor(private bottomSheet: MatBottomSheet, private settingsService: SettingsService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit() {
    this.spanRow = true;
  }
}
