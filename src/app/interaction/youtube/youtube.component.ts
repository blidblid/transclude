import { Component, OnInit } from '@angular/core';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { MatBottomSheet } from '@angular/material';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-youtube', //tslint:disable-line
  templateUrl: './youtube.component.html',
  styleUrls: ['./youtube.component.css']
})
export class YoutubeComponent extends ConfigurableComponent implements OnInit {

  explanation = 'An embedded Youtube player.';
  name = 'Youtube';

  enableComponentEdit: Observable<boolean>;

  constructor(private bottomSheet: MatBottomSheet, private settingsService: SettingsService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit(): void {
    this.enableComponentEdit = this.settingsService.enableComponentEdit;
  }
}
