import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { SettingsService } from '../../main/settings-manager/settings.service';
import { ConfigurableComponent } from '../../main/component-manager/configurable.component';

@Component({
  selector: 'app-paragraph', //tslint:disable-line
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.css']
})
export class ParagraphComponent extends ConfigurableComponent implements OnInit {

  explanation = 'A paragraph that holds text.';
  name = 'Paragraph';

  constructor(private bottomSheet: MatBottomSheet, private settingsService: SettingsService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit() {
    this.spanRow = true;
  }
}
