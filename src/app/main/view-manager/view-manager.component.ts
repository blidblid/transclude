import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';

import { ViewComponent } from '../../layout/view/view.component';
import { StateService } from '../state-manager/state.service';


@Component({
  templateUrl: './view-manager.component.html',
  styleUrls: ['./view-manager.component.css']
})
export class ViewManagerComponent implements OnInit {

  newViewForm: FormGroup;

  navigateForm: FormGroup;

  availableViews: string[] = [];

  constructor(
    private router: Router,
    private stateService: StateService,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) { }

  /** Adds a new view to the app reachable with a specified path. */
  addView(): void {
    let config = this.router.config;
    if (config[0] && config[0].path === '**') {
      config = [];
    }
    const path = this.newViewForm.value.path.toLowerCase();

    if (!config.some(child => child.path === path)) {

      // Register state
      this.stateService.insertNode({
        id: path,
        name: 'View',
        children: [],
        config: {},
        path: path
      });

      // Update routing
      config.push({ path, component: ViewComponent });
      this.router.resetConfig(config);
      this.router.navigateByUrl(path);

      this.setAvailableViews();
      this.navigateForm.setValue({ path });
    }
  }

  private buildForms(): void {
    this.newViewForm = new FormGroup({
      path: new FormControl('', Validators.required)
    });
    this.navigateForm = new FormGroup({
      path: new FormControl('', Validators.required)
    });
  }

  private setAvailableViews(): void {
    this.availableViews = this.router.config.filter(obj => obj.path).map(obj => obj.path);
  }

  private subscribeToNavigate(): void {
    this.navigateForm.valueChanges.subscribe(nav => this.router.navigateByUrl(nav.path));
  }

  ngOnInit(): void {
    this.buildForms();
    this.setAvailableViews();
    this.subscribeToNavigate();
  }
}
