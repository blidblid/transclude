import { Component, ViewChild, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material';
import { Observable } from 'rxjs';

import { ViewManagerComponent } from './view-manager/view-manager.component';
import { SettingsManagerComponent } from './settings-manager/settings-manager.component';
import { SidenavComponent } from '../layout/sidenav/sidenav.component';
import { StateManagerComponent } from './state-manager/state-manager.component';
import { SettingsService } from './settings-manager/settings.service';
import { FirebaseManagerComponent } from './firebase-manager/firebase-manager.component';
import { StateService } from './state-manager/state.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-main', //tslint:disable-line
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.css']
})
export class MainComponent implements OnInit {

  showSidenav: Observable<boolean>;
  showToolbar: Observable<boolean>;
  editableComponents: Observable<boolean>;
  reverseSidenavOpener: Observable<boolean>;
  toolbarConfig: Observable<any>;
  sidenavConfig: Observable<any>;

  @ViewChild(SidenavComponent) sidenav: SidenavComponent;

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
  }

  openViewManager(): void {
    this.bottomSheet.open(ViewManagerComponent);
  }

  openStateManager(): void {
    this.bottomSheet.open(StateManagerComponent);
  }

  openSettingsManager(): void {
    this.bottomSheet.open(SettingsManagerComponent);
  }

  openFirebaseManager(): void {
    this.bottomSheet.open(FirebaseManagerComponent);
  }

  toggleComponentEdit(): void {
    this.settingsService.toggleComponentEdit();
  }

  ngOnInit(): void {
    this.showSidenav = this.settingsService.showSidenav;
    this.showToolbar = this.settingsService.showToolbar;
    this.editableComponents = this.settingsService.enableComponentEdit;
    this.sidenavConfig = this.stateService.stateById('sidenav').pipe(map(node => node.config));
    this.toolbarConfig = this.stateService.stateById('toolbar').pipe(map(node => node.config));
    this.reverseSidenavOpener = this.sidenavConfig.pipe(map(config => config.position.value === 'end'));
  }
}

