import { Component } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { of } from 'rxjs';

import { StateService, StateNode } from './state.service';


@Component({
  templateUrl: './state-manager.component.html',
  styleUrls: ['./state-manager.component.css']
})
export class StateManagerComponent {

  nestedTreeControl: NestedTreeControl<StateNode>;
  nestedDataSource: MatTreeNestedDataSource<StateNode>;

  hasNestedChild = (_: number, nodeData: StateNode) => Array.isArray(nodeData.children) && nodeData.children.length;
  private getChildren = (node: StateNode) => of(node.children);

  constructor(private stateService: StateService) {
    this.nestedTreeControl = new NestedTreeControl<StateNode>(this.getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();
    this.nestedDataSource.data = this.stateService.state.value;
  }
}
