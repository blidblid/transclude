import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { FirebaseService } from '../firebase-manager/firebase.service';
import { Router } from '@angular/router';
import { ViewComponent } from '../../layout/view/view.component';


@Injectable({
  providedIn: 'root'
})
export class StateService {

  /** The tree that represents the app. */
  get state(): BehaviorSubject<StateNode[]> {
    return this._state;
  }
  private _state: BehaviorSubject<StateNode[]> = new BehaviorSubject(initialState.slice(0));

  /** Name of the app. */
  get name(): BehaviorSubject<string> {
    return this._name;
  }
  private _name: BehaviorSubject<string> = new BehaviorSubject('');

  /** Map connecting component ids and filled value. */
  private _values: Map<string, any> = new Map();

  private componentCounter = 0;

  constructor(private title: Title, private firebaseService: FirebaseService, private router: Router) { }

  loadApp(app: App): void {
    this.state.next(app.state);
    this.name.next(app.name);
    this.componentCounter = app.componentCounter;
    this.title.setTitle(app.name);
    this.setRoutingConfig();
  }

  setName(name: string): void {
    this.name.next(name);
  }

  getApp(): App {
    return {
      state: this.state.value,
      name: this.name.value,
      componentCounter: this.componentCounter
    };
  }

  /** Generates a unique ID for a component. */
  generateComponentId(): string {
    return `id-${++this.componentCounter}`;
  }

  save(): void {
    this.state.next(this.state.value);
    this.updateUserApp();
  }

  stateById(id: string): Observable<StateNode> {
    return this.state.pipe(map(() => this.findNode(id)));
  }

  resetApp(): void {
    this.name.next('');
    this.state.next(initialState.slice(0));
    this.router.resetConfig([]);
    this.router.navigateByUrl('');
    this.componentCounter = 0;
  }

  /** Inserts a node to the state tree. */
  insertNode(node: StateNode, parentId?: string): void {
    const state = this.state.value;

    if (this.findNode(node.id)) {
      return;
    }

    if (parentId === undefined) {
      state.push(node);
    } else {
      const parentNode = this.findNode(parentId);
      if (parentNode && parentNode.children) {
        parentNode.children.push(node);
      } else {
        state.push(node);
      }
    }

    this.state.next(state);
    this.updateUserApp();
  }

  /** Removes a node from a state tree by id. */
  removeNode(id: string, tree: StateNode[] = this.state.value) {
    for (let i = 0; i < tree.length; i++) {
      if (tree[i].id === id) {
        tree.splice(i, 1);
        this.state.next(this.state.value);
        this.updateUserApp();
        break;
      } else if (tree[i].children && tree[i].children.length) {
        this.removeNode(id, tree[i].children);
      }
    }
  }

  /** Returns a state node by its id or null if its not found. */
  findNode(id: string): StateNode {
    let stack: StateNode[] = [];
    stack.push(...this.state.value);

    while (stack.length) {
      const node = stack.pop();
      if (node.id === id) {
        return node;
      } else if (node.children) {
        for (let child of node.children) {
          stack.push(child);
        }
      }
    }
    return null;
  }

  private setRoutingConfig(): void {
    const routeConfig = [];
    this.state.value.forEach(state => {
      if (state.path) {
        routeConfig.push({ path: state.path, component: ViewComponent });
      }
    });

    if (routeConfig.length > 0) {
      this.router.resetConfig(routeConfig);
      this.router.navigateByUrl(routeConfig[0].path);
    }
  }

  private updateUserApp(): void {
    if (this.name.value) {
      this.firebaseService.updateUserApp(this.getApp());
    }
  }

  /** Sets a value in the value map. */
  setValue(key: string, value: any) {
    this._values.set(key, value);
  }

  /** Gets a value from the value map. */
  getValue(key: string): any {
    this._values.get(key);
  }

  /** Finds nodes by their names. */
  findNodesByComponentNames(names: string[]): StateNode[] {
    let stack: StateNode[] = [];
    let result: StateNode[] = [];
    stack.push(...this.state.value);

    while (stack.length) {
      const node = stack.pop();
      if (names.some(name => node.name === name)) {
        result.push(node);
      } else if (node.children) {
        for (let child of node.children) {
          stack.push(child);
        }
      }
    }
    return result;
  }
}

export class StateNode {
  id: string;
  name: string;
  config: any;
  children: StateNode[];
  path?: string;
}

export interface App {
  name: string;
  state: StateNode[];
  componentCounter: number;
}

/** The state tree of a new app. */
const initialState = [
  {
    id: 'sidenav',
    name: 'sidenav',
    children: [],
    config: {
      padding: {
        value: 16,
        key: 'padding',
        name: 'Padding',
        type: 'number'
      },
      position: {
        value: 'start',
        key: 'position',
        name: 'Position',
        type: 'option',
        options: [
          'start',
          'end'
        ]
      }
    }
  },
  {
    id: 'toolbar',
    name: 'toolbar',
    children: [],
    config: {
      color: {
        value: 'primary',
        name: 'Color',
        key: 'color',
        type: 'option',
        options: [
          'none',
          'primary',
          'accent',
          'warn'
        ]
      }
    }
  }
];
