import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { StateService, StateNode } from './state-manager/state.service';


@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor(private router: Router, private stateService: StateService) { }

  /** Exectues an action. */
  run(action: Action) {
    if (action.action === 'Navigate') {
      this.router.navigateByUrl(action.target.path);
    }
  }

  findTargetsByAction(actionName: string): StateNode[] {
    if (actionName === 'Navigate') {
      return this.stateService.findNodesByComponentNames(['View']);
    }
    return [];
  }
}

export interface Action {
  action: 'Navigate';
  target: StateNode;
}
