import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

import { FirebaseService } from './firebase.service';
import { StateService, App } from '../state-manager/state.service';
import { User } from 'firebase';


@Component({
  templateUrl: './firebase-manager.component.html',
  styleUrls: ['./firebase-manager.component.css']
})
export class FirebaseManagerComponent {

  manageAppForm: FormGroup;

  createAppForm: FormGroup;

  authenticationForm: FormGroup;

  get user(): Observable<User | null> {
    return this.firebaseService.user;
  }

  get userApps(): Observable<App[]> {
    return this.firebaseService.userApps;
  }


  constructor(private firebaseService: FirebaseService, private stateService: StateService) {
    this.buildForm();
  }

  resetApp(): void {
    this.stateService.resetApp();
  }

  setUserApp(): void {
    if (this.createAppForm.valid) {
      this.stateService.setName(this.createAppForm.value.name);
      this.firebaseService.setUserApp(this.stateService.getApp());
    }
  }

  deleteUserApp(): void {
    if (this.manageAppForm.valid) {
      this.firebaseService.deleteUserApp(this.manageAppForm.value.app.name);
      this.stateService.resetApp();
    }
  }

  signIn(): void {
    if (this.authenticationForm.valid) {
      this.firebaseService.signIn(this.authenticationForm.value.email, this.authenticationForm.value.password);
    }
  }

  createUser(): void {
    if (this.authenticationForm.valid) {
      this.firebaseService.createUser(this.authenticationForm.value.email, this.authenticationForm.value.password);
    }
  }

  private buildForm(): void {
    this.createAppForm = new FormGroup({
      name: new FormControl('', [Validators.required])
    });


    this.manageAppForm = new FormGroup({
      app: new FormControl('', [Validators.required])
    });


    this.manageAppForm.get('app').valueChanges.subscribe(app =>  this.stateService.loadApp(app));

    this.authenticationForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
  }
}
