import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirebaseManagerComponent } from './firebase-manager.component';

describe('SheetComponent', () => {
  let component: FirebaseManagerComponent;
  let fixture: ComponentFixture<FirebaseManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirebaseManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirebaseManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
