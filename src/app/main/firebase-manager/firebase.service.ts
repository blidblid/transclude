import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap, tap, shareReplay } from 'rxjs/operators';

import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { App } from '../state-manager/state.service';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService implements OnInit {

  private userId: string;

  constructor(private db: AngularFirestore, private authentication: AngularFireAuth) {
    this.buildObservables();
  }

  /*
   * Writing to database
   * START
   */
  setUserApp(app: App) {
    this.db.collection('users')
      .doc<App>(this.userId)
      .collection('apps')
      .doc<App>(app.name).set(app);
  }

  updateUserApp(app: App) {
    this.db.collection('users')
      .doc<App>(this.userId)
      .collection('apps')
      .doc<App>(app.name).update(app);
  }

  deleteUserApp(name: string) {
    this.db.collection('users')
      .doc<App>(this.userId)
      .collection('apps')
      .doc<App>(name).delete();
  }
  /*
   * END
   * Writing to database
   */


  /*
   * Reading from database
   * START
   */
  userApps: Observable<App[]>;

  buildObservables(): void {
    this.userApps = this.user.pipe(
      tap(user => this.userId = user.uid),
      mergeMap(user => {
        return this.db.collection('users')
          .doc<App>(user.uid)
          .collection<App>('apps').valueChanges();
      }),
      shareReplay()
    );
  }
  /*
   * Reading from database
   * END
   */


  /*
   * Authentication
   * START
   */
  signIn(email: string, password: string): void {
    this.authentication.auth.signInWithEmailAndPassword(email, password);
  }

  signOut(): void {
    this.authentication.auth.signOut();
  }

  createUser(email: string, password: string): void {
    this.authentication.auth.createUserWithEmailAndPassword(email, password).then(user => {
      this.db.collection('users').doc(user.user.uid).set({ apps: [] });
    });
  }

  get user(): Observable<User | null> {
    return this.authentication.user;
  }
  /*
   * END
   * Authentication
   */

  ngOnInit(): void {
  }
}
