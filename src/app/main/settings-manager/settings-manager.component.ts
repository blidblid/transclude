import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { SettingsService } from './settings.service';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';


@Component({
  templateUrl: './settings-manager.component.html',
  styleUrls: ['./settings-manager.component.css']
})
export class SettingsManagerComponent implements OnInit {

  settings: FormGroup;

  constructor(private settingService: SettingsService) { }

  buildForm(): void {
    this.settings = new FormGroup({
      disableEdit: new FormControl(this.settingService.disableEdit.value),
      showSidenav: new FormControl(this.settingService.showSidenav.value),
      showToolbar: new FormControl(this.settingService.showToolbar.value)
    });
  }

  ngOnInit(): void {
    this.buildForm();
    this.settings.valueChanges.subscribe(value => this.settingService.updateSettingsFromForm(value));
  }
}
