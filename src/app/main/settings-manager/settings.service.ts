import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  /** whether the app should be editable or not. */
  get disableEdit(): BehaviorSubject<boolean> {
    return this._disableEdit;
  }
  private _disableEdit: BehaviorSubject<boolean> = new BehaviorSubject(false);

  /** whether the app should have a sidenav or not. */
  get showSidenav(): BehaviorSubject<boolean> {
    return this._showSidenav;
  }
  private _showSidenav: BehaviorSubject<boolean> = new BehaviorSubject(true);

  /** whether the app should have a toolbar or not. */
  get showToolbar(): BehaviorSubject<boolean> {
    return this._showToolbar;
  }
  private _showToolbar: BehaviorSubject<boolean> = new BehaviorSubject(true);

  /** whether the app should be editable or not. */
  get enableComponentEdit(): BehaviorSubject<boolean> {
    return this._enableComponentEdit;
  }
  private _enableComponentEdit: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

  /** Reads a form and emits values if the key is a member of this service. */
  updateSettingsFromForm(value: any) {
    Object.keys(value).forEach(key => {
      if (this['_' + key]) {
        this['_' + key].next(value[key]);
      }
    });
  }

  toggleComponentEdit(): void {
    this._enableComponentEdit.next(!this._enableComponentEdit.value);
  }
}
