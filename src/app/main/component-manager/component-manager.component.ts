import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

import { StateNode, StateService } from '../state-manager/state.service';
import { ActionService } from '../action.service';


@Component({
  templateUrl: './component-manager.component.html',
  styleUrls: ['./component-manager.component.css']
})
export class ComponentManagerComponent implements OnDestroy, OnInit {

  configChange: Subject<any> = new Subject();

  /** Generated form from all configurable options. */
  configForm: FormGroup;

  /** All available configurations. */
  configurations: any[] = [];

  /** Available targets of actions. */
  actionTargets: StateNode[] = [];

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private stateService: StateService,
    private bottomSheetRef: MatBottomSheetRef,
    private actionService: ActionService) { }

  addAction(): void {
    if (this.configForm.value.action && this.configForm.value.actionTarget) {
      this.data.config.action.actions.push({
        action: this.configForm.value.action,
        target: this.configForm.value.actionTarget
      });
    }
  }

  removeAction(index: number): void {
    this.data.config.action.actions.splice(index, 1);
  }

  /** Closes the bottom sheet. */
  close(): void {
    this.bottomSheetRef.dismiss();
  }

  /** Builds the config form from the FormConfig. */
  private buildConfigForm(): void {
    // Dynamically build a form
    let group = {};
    Object.keys(this.data.config).forEach(key => {
      const config = this.data.config[key];
      this.configurations.push(config);
      group[config.key] = new FormControl(config.value);
    });

    group['actionTarget'] = new FormControl();

    this.configForm = new FormGroup(group);

    // Update the Injected data with form values.
    this.configForm.valueChanges.subscribe(value => {
      Object.keys(this.data.config).forEach(key => this.data.config[key].value = value[key]);
      this.configChange.next(value);
    });

    if (this.configForm.get('action')) {
      this.configForm.get('action').valueChanges.subscribe(action => {
        this.actionTargets = this.actionService.findTargetsByAction(action);
      });
    }
  }

  ngOnInit() {
    this.buildConfigForm();
  }

  ngOnDestroy() {
    this.stateService.save();
  }
}
