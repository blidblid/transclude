import { HostBinding, HostListener, Input } from '@angular/core';
import { MatBottomSheet } from '@angular/material';
import { takeUntil } from 'rxjs/operators';

import { ComponentManagerComponent } from './component-manager.component';
import { SettingsService } from '../settings-manager/settings.service';


/** All common behavior of configurable components, such as opening a bottom sheet. */
export abstract class ConfigurableComponent {

  /** Unique id of the component. */
  @Input()
  id: string;

  /** Configurations of the component. */
  @Input()
  config: any;

  /** Destroy this component. */
  @Input()
  destroy: () => void;

  /** Explanation of the component. */
  abstract explanation: string;

  /** Name of the component. */
  abstract name: string;

  @HostBinding('class.editable')
  editable: boolean;

  @HostBinding('class.grid-span-1')
  spanOne: boolean = false;

  @HostBinding('class.grid-span-2')
  spanTwo: boolean = false;

  @HostBinding('class.grid-span-3')
  spanThree: boolean = false;

  @HostBinding('class.grid-span-4')
  spanFour: boolean = false;

  @HostBinding('class.grid-span-row')
  spanRow: boolean = false;

  constructor(private _bottomSheet: MatBottomSheet, private _settingsService: SettingsService) {
    this.subscribeToCommonSettings();
  }

  @HostListener('click', ['$event'])
  openComponentManager(event: Event): void {
    if (this._settingsService.enableComponentEdit.value) {
      const bottomSheetRef = this._bottomSheet.open(ComponentManagerComponent, {
        data: {
          config: this.config,
          name: this.name,
          id: this.id,
          destroy: this.destroy
        }
      });

      bottomSheetRef.instance.configChange.pipe(takeUntil(bottomSheetRef.afterDismissed())).subscribe(config => {
        if (config.spanColumns) {
          this.setSpanClass(config.spanColumns);
        }
      });

      event.preventDefault();
      event.stopPropagation();
    }
  }

  private subscribeToCommonSettings(): void {
    this._settingsService.enableComponentEdit.subscribe(enableComponentEdit => this.editable = enableComponentEdit);
  }

  private setSpanClass(spans: string) {
    this.spanOne = this.spanTwo = this.spanThree = this.spanFour = this.spanRow = false;
    if (spans === 'One column') {
      this.spanOne = true;
    } else if (spans === 'Two columns') {
      this.spanTwo = true;
    } else if (spans === 'Three columns') {
      this.spanThree = true;
    } else if (spans === 'Four columns') {
      this.spanFour = true;
    } else if (spans === 'Entire row') {
      this.spanRow = true;
    }
  }
}
