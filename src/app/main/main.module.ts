import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MainComponent } from './main.component';
import { ViewManagerComponent } from './view-manager/view-manager.component';
import { ComponentManagerComponent } from './component-manager/component-manager.component';
import { SettingsManagerComponent } from './settings-manager/settings-manager.component';
import { LayoutModule } from '../layout/layout.module';
import { StateManagerComponent } from './state-manager/state-manager.component';
import { ViewComponent } from '../layout/view/view.component';
import { MaterialModule } from './material.module';
import { FirebaseManagerComponent } from './firebase-manager/firebase-manager.component';
import { InteractionModule } from '../interaction/interaction.module';


@NgModule({
  declarations: [
    ComponentManagerComponent,
    FirebaseManagerComponent,
    MainComponent,
    SettingsManagerComponent,
    StateManagerComponent,
    ViewManagerComponent
  ],
  entryComponents: [
    ComponentManagerComponent,
    FirebaseManagerComponent,
    SettingsManagerComponent,
    StateManagerComponent,
    ViewManagerComponent
  ],
  exports: [
    MainComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    InteractionModule,
    LayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forRoot([{ path: '**', pathMatch: 'full', redirectTo: '' }])
  ]
})
export class MainModule { }
