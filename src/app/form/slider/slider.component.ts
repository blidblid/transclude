import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { MatBottomSheet } from '@angular/material';
import { StateService } from '../../main/state-manager/state.service';

@Component({
  selector: 'app-slider', //tslint:disable-line
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SliderComponent extends ConfigurableComponent implements OnInit {

  name = 'Slider';
  explanation = 'Select to select a continuous value.';

  set value(value: any) {
    this.stateService.setValue(this.id, value);
  }

  get value(): any {
    return this.stateService.getValue(this.id);
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit() { }

}
