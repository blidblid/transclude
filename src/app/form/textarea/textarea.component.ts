import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { StateService } from '../../main/state-manager/state.service';


@Component({
  selector: 'app-textarea', //tslint:disable-line
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextareaComponent extends ConfigurableComponent {

  name = 'Textarea';
  explanation = 'Textarea to be used in forms.';

  set value(value: any) {
    this.stateService.setValue(this.id, value);
  }

  get value(): any {
    return this.stateService.getValue(this.id);
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }
}

