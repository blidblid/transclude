import { Component, ChangeDetectionStrategy } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { StateService } from '../../main/state-manager/state.service';


@Component({
  selector: 'app-input', //tslint:disable-line
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent extends ConfigurableComponent {

  name = 'Input';
  explanation = 'Input field to be used in forms';

  set value(value: any) {
    this.stateService.setValue(this.id, value);
  }

  get value(): any {
    return this.stateService.getValue(this.id);
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }
}
