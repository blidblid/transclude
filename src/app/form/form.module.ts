import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { InputComponent } from './input/input.component';
import { TextareaComponent } from './textarea/textarea.component';
import { SliderComponent } from './slider/slider.component';
import { SlideToggleComponent } from './slide-toggle/slide-toggle.component';
import { MaterialModule } from '../main/material.module';


const components = [
  InputComponent,
  SliderComponent,
  SlideToggleComponent,
  TextareaComponent
];

@NgModule({
  declarations: components,
  exports: components,
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class FormModule { }
