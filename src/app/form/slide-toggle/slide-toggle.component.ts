import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { ConfigurableComponent } from '../../main/component-manager/configurable.component';
import { SettingsService } from '../../main/settings-manager/settings.service';
import { StateService } from '../../main/state-manager/state.service';

@Component({
  selector: 'app-slide-toggle', //tslint:disable-line
  templateUrl: './slide-toggle.component.html',
  styleUrls: ['./slide-toggle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideToggleComponent extends ConfigurableComponent implements OnInit {

  name = 'Slide toggle';
  explanation = 'Slide toggle that for options that are true or false.';

  set value(value: any) {
    this.stateService.setValue(this.id, value);
  }

  get value(): any {
    return this.stateService.getValue(this.id);
  }

  constructor(
    private bottomSheet: MatBottomSheet,
    private settingsService: SettingsService,
    private stateService: StateService) {
    super(bottomSheet, settingsService);
  }

  ngOnInit() { }
}
